<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Backup extends Model
{
    protected $fillable = ['user_id', 'password'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

}
