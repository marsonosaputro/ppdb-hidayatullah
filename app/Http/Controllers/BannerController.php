<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Banner;
use Image;
use Session;
use File;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['banner'] = Banner::orderBy('publish', 'Y')->paginate(5);
        return view('banner.index', $data)->with('no', $data['banner']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Banner';
        return view('banner.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'judul' => 'required',
          'image' => 'required|image',
          'url' => 'sometimes|url'
        ]);
        if(!empty($request->file('image'))){
          $image = time().$request->file('image')->getClientOriginalName();
          $request->file('image')->move('images/banner/', $image);
          Image::make(public_path().'/images/banner/'.$image)->crop(250,400)->save();
        }else{
          $image = '';
        }
        $data = $request->all();
        $data['image'] = $image;
        Banner::create($data);
        Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Berhasil menambahkan Banner <b>'.$request['judul'].'</b>'
              ]);
        return redirect(route('banner.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Update Banner';
        $data['banner'] = Banner::findOrFail($id);
        return view('banner.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'judul' => 'required',
        'image' => 'image',
        'url' => 'sometimes|url'
      ]);
      $banner = Banner::findOrFail($id);
      if(!empty($request->file('image'))){
        $image = time().$request->file('image')->getClientOriginalName();
        $request->file('image')->move('images/banner/', $image);
        Image::make(public_path().'/images/banner/'.$image)->crop(250,400)->save();
        File::delete('images/banner/'.$banner->image);
      }else{
        $image = $banner->image;
      }
      $data = $request->all();
      $data['image'] = $image;
      $banner->update($data);
      Session::flash('flash_notification', [
              'level'=>'info',
              'message'=>'Berhasil mengubah Banner ke <b>'.$request['judul'].'</b>'
            ]);
      return redirect(route('banner.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        Session::flash('flash_notification', [
                'level'=>'danger',
                'message'=>'Berhasil menghapus Banner <b>'.$banner->judul.'</b>'
              ]);
        File::delete('images/banner/'.$banner->image);
        $banner->delete();
        return redirect(route('banner.index'));

    }
}
