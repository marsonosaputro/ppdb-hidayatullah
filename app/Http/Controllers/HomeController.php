<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Slideshow;
use App\Statis;
use App\Ortu;
use App\Siswa;
use App\User;
use DB;
use Excel;
use Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['slide'] = Slideshow::all()->take(2);
        $data['active'] = Slideshow::all()->last();
        $data['alur'] = Statis::where('id', 1)->first();
        return view('home', $data);
    }

    public function prosedur()
    {
      $data['prosedur'] = Statis::where('id', 2)->first();
      return view('homepage.prosedur', $data);
    }

    public function agenda()
    {
      $data['agenda'] = Statis::where('id', 3)->first();
      return view('homepage.agenda', $data);
    }

    public function dataPendaftar($jenjang='')
    {
      if($jenjang == 'mts')
      {
        $data['siswa'] = Siswa::where('jenjang', 'MTS')->paginate(10);
      }
      elseif ($jenjang == 'ma')
      {
        $data['siswa'] = Siswa::where('jenjang', 'MA')->paginate(10);
      }
      else
      {
        $data['siswa'] = Siswa::orderBy('id', 'asc')->paginate(10);
      }
      return view('datapendaftar', $data)->with('no', $data['siswa']->firstItem());
    }

    public function dashboard()
    {
      $data['admin'] = Statis::where('id', '4')->first();
      $data['user'] = Statis::where('id', '5')->first();
      $data['syarat'] = Statis::where('id', '7')->first();
      $data['siswa'] = Siswa::where('user_id',Auth::user()->id)->first();
      return view('dashboard', $data);
    }

    public function backup()
    {
      $data['user'] = DB::table('users')
                          ->leftJoin('backups', 'users.id', '=', 'backups.user_id')
                          ->get();
      return view('backup', $data)->with('no', 1);
    }

    public function viewData($id='')
    {
      $data['siswa'] = Siswa::where('user_id', $id)->first();
      $data['ortu'] = Ortu::where('user_id', $id)->first();
      return view('viewdata', $data);
    }

    public function ubahStatus(Request $request)
    {
      DB::table('statuses')->where('siswa_id', $request['siswa_id'])->update(['status'=>$request['status']]);
      return redirect('viewdata/'.$request['user_id']);
    }

    public function downloadExcel($jenjang='')
    {
      $siswa = DB::table('siswas')
                        ->join('ortus', 'siswas.user_id', '=', 'ortus.user_id')
                        ->join('statuses', 'siswas.id', '=', 'statuses.siswa_id')
                        ->where('statuses.status', '1')
                        ->where('siswas.jenjang', 'MTS')->get();
      $siswama = DB::table('siswas')
                          ->join('ortus', 'siswas.user_id', '=', 'ortus.user_id')
                          ->join('statuses', 'siswas.id', '=', 'statuses.siswa_id')
                          ->where('statuses.status', '1')
                          ->where('siswas.jenjang', 'MA')->get();

      Excel::create('Data Pendaftar', function($excel) use ($siswa, $siswama){
        $excel->setTitle('Data Pendaftar')->setCreator(Auth::user()->name);
        $excel->sheet('Data Pendaftar MTs', function($sheet) use ($siswa){
          $row = 1;
          $no = 1;
          $sheet->row($row, ['No', 'Jenjang', 'Nama Lengkap', 'Tempat Lahir', 'Tgl Lahir', 'Alamat Lengkap', 'Kode Pos', 'Asal Sekolah', 'Alamat Sekolah', 'Thn Lulus', 'No.HP', 'email', 'Nama Ayah', 'Nama Nama Ibu', 'Nama Wali', 'Alamat Rumah', 'Profesi']);
          foreach ($siswa as $d) {
            $sheet->row(++$row, [$no++, $d->jenjang, $d->nama, $d->tempatlhr, $d->tgllahir, $d->alamatlkp, $d->kodepos, $d->asal, $d->alamatskl, $d->thnlulus, $d->hp, $d->email, $d->ayah, $d->ibu, $d->wali, $d->alamatrmh, $d->profesi]);
          }
        });
        $excel->sheet('Data MA', function($sheet) use ($siswama){
          $row = 1;
          $no = 1;
          $sheet->row($row, ['No', 'Jenjang', 'Nama Lengkap', 'Tempat Lahir', 'Tgl Lahir', 'Alamat Lengkap', 'Kode Pos', 'Asal Sekolah', 'Alamat Sekolah', 'Thn Lulus', 'No.HP', 'email', 'Nama Ayah', 'Nama Nama Ibu', 'Nama Wali', 'Alamat Rumah', 'Profesi']);
          foreach ($siswama as $d) {
            $sheet->row(++$row, [$no++, $d->jenjang, $d->nama, $d->tempatlhr, $d->tgllahir, $d->alamatlkp, $d->kodepos, $d->asal, $d->alamatskl, $d->thnlulus, $d->hp, $d->email, $d->ayah, $d->ibu, $d->wali, $d->alamatrmh, $d->profesi]);
          }
        });
      })->export('xlsx');

    }
}
