<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Ortu;

class OrtuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['ortu'] = Ortu::where('user_id',Auth::user()->id)->first();
        return view('ortu.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ortu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'ayah' => 'required',
          'ibu' => 'required',
          'alamatrmh' => 'required',
          'profesi'=> 'required'
        ]);
        Ortu::create($request->all());
        return redirect('datalengkap');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['ortu'] = Ortu::findOrFail($id);
        return view('ortu.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['ortu'] = Ortu::findOrFail($id);
        return view('ortu.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'ayah' => 'required',
          'ibu' => 'required',
          'alamatrmh' => 'required',
          'profesi'=> 'required'
        ]);
        Ortu::findOrFail($id)->update($request->all());
        return redirect('ortu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ortu::findOrFail($id)->delete();
        return redirect('ortu');
    }
}
