<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Setting;
use Image;
use File;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $setting = Setting::where('id', '1')->first();
      return view('setting.index', compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);
        return view('setting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $etting = Setting::findOrFail($id);
        if(!empty($request->file('logo'))){
          $image = time().$request->file('logo')->getClientOriginalName();
          $request->file('logo')->move('images/', $image);
          $img = Image::make(public_path().'/images/'.$image)->resize(850,236)->save();
          File::delete(public_path().'/images/'.$etting->logo);
        }else{
          $image = $etting->logo;
        }

        if(!empty($request->file('logoma'))){
          $image2 = time().$request->file('logoma')->getClientOriginalName();
          $request->file('logoma')->move('images/', $image2);
          $img = Image::make(public_path().'/images/'.$image2)->resize(850,236)->save();
          File::delete(public_path().'/images/'.$etting->logoma);
        }else{
          $image2 = $etting->logoma;
        }

        $data = $request->all();
        $data['logo'] = $image;
        $data['logoma'] = $image2;
        $etting->update($data);
        return redirect('setting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
