<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\SimpansiswaRequest;
use Illuminate\Support\Facades\Auth;
use App\Siswa;
use App\User;
use App\Ortu;
use App\Statis;
use App\Status;
use PDF;
use Image;
use File;


class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['siswa'] = Siswa::where('user_id',Auth::user()->id)->first();
      return view('siswa.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SimpansiswaRequest $request)
    {
      if(!empty($request->file('foto')))
      {
        $image = time().$request->file('foto')->getClientOriginalName(); //ambil nama, simpan divariable $image
        $request->file('foto')->move('foto/', $image); //set upload ke folder
        Image::make(public_path().'/foto/'.$image)->resize(150,200)->save();
      }else {
        $image = "";
      }

      //Simpan Data Siswa
      $data = $request->all();
      $data['tgllahir'] = kalender($request['tgllahir']);
      $data['foto'] = $image;
      $siswa = Siswa::create($data);

      $status = new Status;
      $status->status = 0;
      $siswa->status()->save($status);

      //Update Data User
      $user['name'] = $request['nama'];
      $user['email'] = $request['email'];
      $siswa->user()->update($user);

      return redirect('ortu')->with('Sukses', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data['siswa'] = Siswa::where('user_id',Auth::user()->id)->first();
      return view('siswa.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['siswa']=Siswa::findOrFail($id);
      return view('siswa.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SimpansiswaRequest $request, $id)
    {
      $siswa = Siswa::findOrFail($id);
      if(!empty($request->file('foto')))
      {
        $image = time().$request->file('foto')->getClientOriginalName(); //ambil nama, simpan divariable $image
        $request->file('foto')->move('foto/', $image); //set upload ke folder
        Image::make(public_path().'/foto/'.$image)->resize(150,200)->save();
        File::delete(public_path().'/foto/'.$siswa->foto);
      }else {
        $image = $siswa->foto;
      }

      //Update Data Siswa
      $data = $request->all();
      $data['tgllahir'] = kalender($request['tgllahir']);
      $data ['foto'] = $image;
      $siswa->update($data);

      //Update Data User
      $user['name'] = $request['nama'];
      $user['email'] = $request['email'];
      $siswa->user()->update($user);
      return redirect('siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Siswa::findOrFail($id)->delete();
      return redirect('siswa');
    }

    public function datalengkap()
    {
      $data['siswa'] = Siswa::where('user_id',Auth::user()->id)->first();
      $data['ortu'] = Ortu::where('user_id',Auth::user()->id)->first();
      return view('siswa.datalengkap', $data);
    }

    public function getPdf($id='')
    {
      $user_id = (! empty($id) ? $id : Auth::user()->id);
      $data['siswa'] = Siswa::where('user_id', $user_id)->first();
      $data['ortu'] = Ortu::where('user_id', $user_id)->first();
      $data['pengumuman'] = Statis::where('id', 6)->first();
      $pdf= PDF::loadView('getPdf',$data);
      return $pdf->download($data['siswa']->nama.'.pdf');
    }
}
