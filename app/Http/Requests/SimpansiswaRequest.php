<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SimpansiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'jenjang' => 'required',
          'nama' => 'required|min:3',
          'tempatlhr' => 'required|min:4',
          'tgllahir' => 'required',
          'alamatlkp' => 'required',
          'kodepos' => 'required|numeric|digits_between:5,10',
          'asal' => 'required',
          'alamatskl' => 'required',
          'thnlulus' => 'required|numeric|digits:4',
          'hp' => 'required|numeric|digits_between:10,15',
          'email' => 'required|email',
          'foto' => 'sometimes|image|mimes:jpeg,png',
        ];

    }
}
