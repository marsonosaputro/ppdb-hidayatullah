<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ortu extends Model
{
    protected $fillable = ['ayah','ibu','wali','alamatrmh','profesi','user_id'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
