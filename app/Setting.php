<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['title', 'description', 'author', 'keyword', 'contact', 'contact1', 'contact2', 'logo', 'logoma'];
}
