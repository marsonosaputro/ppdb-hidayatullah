<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Status;

class Siswa extends Model
{
    protected $fillable = ['jenjang','nama','tempatlhr','tgllahir','alamatlkp','kodepos','asal','alamatskl','thnlulus','hp','email','foto','user_id'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function status()
    {
      return $this->hasOne(Status::class);
    }

}
