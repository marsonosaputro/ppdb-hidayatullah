<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{
    protected $fillable = ['judul', 'keterangan', 'keterangan2', 'link', 'image', 'publish']; 
}
