<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Statis extends Model
{
    protected $fillable = ['kategoristatis_id' ,'judul', 'slug', 'content', 'image', 'hits', 'penulis'];

    public function kategoristatis()
    {
      return $this->belongsTo('App\Kategoristatis');
    }
}
