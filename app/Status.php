<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Siswa;

class Status extends Model
{
    protected $fillable = ['siswa_id', 'status'];
    
    public function siswa()
    {
      return $this->belongsTo(Siswa::class);
    }
}
