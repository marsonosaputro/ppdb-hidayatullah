<?php

if ( ! function_exists('hari_ini'))
{
	function hari_ini()
	{
		date_default_timezone_set('Asia/Jakarta');
        $seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
        $hari = date("w");
        $hari_ini = $seminggu[$hari];
        return $hari_ini;
	}
}

if ( ! function_exists('tgl_indo'))
{
	function tgl_indo($tanggal)
	{
			$t   = explode('-', $tanggal);
	    switch ($t[1])
	        {
	            case '01'  : $b = 'Januari'; break;
	            case '02'  : $b = 'Februari'; break;
	            case '03'  : $b = 'Maret'; break;
	            case '04'  : $b = 'April'; break;
	            case '05'  : $b = 'Mei'; break;
	            case '06'  : $b = 'Juni'; break;
	            case '07'  : $b = 'Juli'; break;
	            case '08'  : $b = 'Agustus'; break;
	            case '09'  : $b = 'September'; break;
	            case '10'  : $b = 'Oktober'; break;
	            case '11'  : $b = 'Nopember'; break;
	            case '12'  : $b = 'Desember'; break;
	        }
	    return $t[2].' '.$b.' '.$t['0'];
	}
}

if ( ! function_exists('setting'))
{
	function setting()
	{
		return DB::table('settings')->where('id','1')->first();
	}
}

function kalender($tgl)
{
    if(!empty($tgl))
    {
        $x = explode(' ', $tgl);
				if(! empty($x[1]) && ($x[1] == 'Januari' || $x[1] == 'Februari' || $x[1] == 'Maret' || $x[1] == 'April'
				|| $x[1] == 'Mei' || $x[1] == 'Juni' || $x[1] == 'Juli' || $x[1] == 'Agustus' || $x[1] == 'September'
				|| $x[1] == 'Oktober' || $x[1] == 'November' || $x[1] == 'Nopember' || $x[1] == 'Desember'))
				{
					switch($x[1])
	        {
	            case 'Januari'   : $b = '01'; break;
	            case 'Februari'  : $b = '02'; break;
	            case 'Maret'     : $b = '03'; break;
	            case 'April'     : $b = '04'; break;
	            case 'Mei'       : $b = '05'; break;
	            case 'Juni'      : $b = '06'; break;
	            case 'Juli'      : $b = '07'; break;
	            case 'Agustus'   : $b = '08'; break;
	            case 'September' : $b = '09'; break;
	            case 'Oktober'   : $b = '10'; break;
	            case 'November'  : $b = '11'; break;
	            case 'Nopember'  : $b = '11'; break;
	            case 'Desember'  : $b = '12'; break;
	        }
	        $tanggal = $x[2].'-'.$b.'-'.$x[0];
	        return $tanggal;
				}
				else
				{
					return date('Y-m-d');
				}

    }
}
