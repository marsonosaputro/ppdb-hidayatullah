<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenjang');
            $table->string('nama');
            $table->string('tempatlhr');
            $table->date('tgllahir');
            $table->string('alamatlkp'); //alamat lengkap
            $table->string('kodepos');
            $table->string('asal');
            $table->string('alamatskl'); //alamat sekolah
            $table->string('thnlulus');
            $table->string('hp');
            $table->string('email');
            $table->string('foto');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('Cascade')->onDelete('Cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
