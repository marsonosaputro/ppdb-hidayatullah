<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        //Create Admin Role
        $adminRole = new Role();
        $adminRole->name = 'admin';
        $adminRole->display_name = 'Admin';
        $adminRole->save();

        //Create User role
        $userRole = new Role();
        $userRole->name = 'user';
        $userRole->display_name = 'Calon Santri Baru';
        $userRole->save();


        //Create Admin User
        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('rahasia');
        $admin->save();
        $admin->attachRole($adminRole);

        //Create User
        $user = new User();
        $user->name = 'Calon Sabtri';
        $user->email = 'user@gmail.com';
        $user->password = bcrypt('rahasia');
        $user->save();
        $user->attachRole($userRole);
    }
}
