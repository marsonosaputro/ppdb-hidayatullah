<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        //Create Admin Role
        $adminRole = new Role();
        $adminRole->name = 'admin';
        $adminRole->display_name = 'Admin';
        $adminRole->save();

        //Create User role
        $userRole = new Role();
        $userRole->name = 'user';
        $userRole->display_name = 'Calon Santri Baru';
        $userRole->save();


        //Create Admin User
        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'ronyoct11@gmail.com';
        $admin->password = bcrypt('faceb00k');
        $admin->save();
        $admin->attachRole($adminRole);

        //Create User
        $user = new User();
        $user->name = 'Staff User';
        $user->email = 'user@gmail.com';
        $user->password = bcrypt('rahasia');
        $user->save();
        $user->attachRole($userRole);
    }
}
