-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 03, 2017 at 03:56 AM
-- Server version: 5.7.12-0ubuntu1.1
-- PHP Version: 7.0.8-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newppdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `backups`
--

INSERT INTO `backups` (`id`, `user_id`, `password`, `created_at`, `updated_at`) VALUES
(1, 3, 'rahasia', '2016-12-31 10:24:54', '2016-12-31 10:24:54'),
(2, 4, 'rahasia', '2016-12-31 10:38:30', '2016-12-31 10:38:30'),
(3, 5, 'faceb00k', '2017-01-01 02:15:28', '2017-01-01 02:15:28'),
(4, 6, 'raihan', '2017-01-02 06:33:30', '2017-01-02 06:33:30'),
(5, 8, 'rahasia', '2017-01-02 11:52:26', '2017-01-02 11:52:26');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `judul`, `image`, `url`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'banner1', '1482396931baner-juara-2.jpg', '', 'Y', '2016-12-22 08:55:31', '2016-12-22 08:55:31'),
(2, 'baner2', '1482397196baner-juara-1.jpg', '', 'Y', '2016-12-22 08:59:56', '2016-12-22 08:59:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(17, '2014_10_12_000000_create_users_table', 1),
(18, '2014_10_12_100000_create_password_resets_table', 1),
(19, '2016_09_14_154839_create_statis_table', 1),
(20, '2016_09_15_090104_create_slideshows_table', 1),
(21, '2016_10_11_072945_create_banners_table', 1),
(22, '2016_12_06_032550_create_siswas_table', 1),
(23, '2016_12_07_021643_create_ortus_table', 1),
(24, '2016_12_07_125746_laratrust_setup_tables', 1),
(25, '2016_12_23_130252_create_backups_table', 2),
(26, '2016_12_31_095407_create_statuses_table', 3),
(27, '2017_01_02_004631_create_settings_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `ortus`
--

CREATE TABLE `ortus` (
  `id` int(10) UNSIGNED NOT NULL,
  `ayah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ibu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wali` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamatrmh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profesi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ortus`
--

INSERT INTO `ortus` (`id`, `ayah`, `ibu`, `wali`, `alamatrmh`, `profesi`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Marsono', 'Retnowati', '-', 'Kismoyoso Ngemplak Boyolali', 'Programmer', 3, '2016-12-31 10:26:57', '2016-12-31 10:26:57'),
(2, 'Marsono', 'Retnowati', '-', 'Kismoyoso', 'Programmer', 4, '2016-12-31 10:40:31', '2016-12-31 10:40:31'),
(3, 'Sutomo', 'Winarsih', '', 'Nogosari', 'Buruh', 5, '2017-01-01 02:17:44', '2017-01-01 02:17:44'),
(4, 'anies', 'anik', 'anis', 'banyuanyar', 'kendal', 6, '2017-01-02 06:37:06', '2017-01-02 06:37:06'),
(5, 'Nama Ayah Santri', 'NAma Ibu Santri', '-', 'Alamat Rumah', 'Profesi santri', 8, '2017-01-02 11:54:45', '2017-01-02 11:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('yusron@gmail.com', '42ceb030f7d647f3adfde17b9e6c79bef74fdef269ccfbcd848f4eb302d6b944', '2016-12-23 07:39:52');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, '2016-12-31 10:23:58', '2016-12-31 10:23:58'),
(2, 'user', 'Calon Santri Baru', NULL, '2016-12-31 10:23:58', '2016-12-31 10:23:58');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(7, 1),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `description`, `author`, `keyword`, `contact`, `contact1`, `contact2`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'PPDB Online', 'PPDB ONline Ponpes Hidayatullah', 'Hidayatullah', 'ppdb', '0987 7654 3749', '0987 7654 3456', '0876 9098 3456', '1483359952kop.jpg', NULL, '2017-01-02 12:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `siswas`
--

CREATE TABLE `siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenjang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tempatlhr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tgllahir` date NOT NULL,
  `alamatlkp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kodepos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamatskl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thnlulus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `siswas`
--

INSERT INTO `siswas` (`id`, `jenjang`, `nama`, `tempatlhr`, `tgllahir`, `alamatlkp`, `kodepos`, `asal`, `alamatskl`, `thnlulus`, `hp`, `email`, `foto`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'MA', 'M. Yusron Adnan', 'Boyolali', '1997-08-14', 'Karangpung RT 01/09 Kismoyoso Ngemplak Boyolali', '57375', 'MI Tambas', 'Tambas Kismoyoso', '2015', '085742324183', 'yusron@gmail.com', '1483179991baner-juara-1.jpg', 3, '2016-12-31 10:26:31', '2017-01-02 14:01:09'),
(2, 'MTS', 'Fayza Ulya Azmina', 'Boyolali', '2012-03-18', 'Kismoyoso ', '57375', 'MI Tambas', 'Tambas', '2015', '098654357899', 'azmina@gmail.com', '1483180805apple_mac_os_x_el_capitan-wide.jpg', 4, '2016-12-31 10:40:06', '2016-12-31 10:40:06'),
(3, 'MA', 'Tri Achmad Syahroni', 'Bekasi', '1994-12-11', 'Sumurwaru, 06/07, Keyongan, Nogosari, Boyolali', '57378', 'SMP N1 NOGOSARI', 'NOGOSARI', '2006', '085726693778', 'triachmadsyahroni@gmail.com', '1483237036baner-juara-2.jpg', 5, '2017-01-01 02:17:16', '2017-01-01 02:17:16'),
(4, 'MTS', 'Raihan', 'Kendal', '2000-06-15', 'Tamanrejo', '66778', 'Tamanrejo 1', 'Limbangan', '2015', '5656787687678', 'raihan@gmail.com', '1483339048apple_mac_os_x_el_capitan-wide.jpg', 6, '2017-01-02 06:36:40', '2017-01-02 06:37:29'),
(5, 'MTS', 'Santri Baru', 'Boyolali', '2003-03-01', 'Alamat lengkap', '67654', 'Asal Sekolah', 'Alamat Sekolah', '2015', '098765335677', 'santri@gmail.com', '1483358057baner-juara-1.jpg', 8, '2017-01-02 11:54:17', '2017-01-02 11:54:17');

-- --------------------------------------------------------

--
-- Table structure for table `slideshows`
--

CREATE TABLE `slideshows` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slideshows`
--

INSERT INTO `slideshows` (`id`, `judul`, `keterangan`, `keterangan2`, `link`, `image`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Slide1', '', '', '', '1483313722611997-linux-mint-wallpaper.jpg', 'Y', '2017-01-01 23:35:22', '2017-01-01 23:35:22'),
(2, 'Slide2', '', '', '', '1483313739apple_mac_os_x_el_capitan-wide.jpg', 'Y', '2017-01-01 23:35:40', '2017-01-01 23:35:40'),
(4, 'Slide3', '', '', '', '1483313803linux-mint-wallpaper-4428-hd-wallpapers.jpg', 'Y', '2017-01-01 23:36:43', '2017-01-01 23:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `statis`
--

CREATE TABLE `statis` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hits` int(11) NOT NULL,
  `penulis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `statis`
--

INSERT INTO `statis` (`id`, `judul`, `slug`, `content`, `image`, `hits`, `penulis`, `created_at`, `updated_at`) VALUES
(1, 'ALUR PENDAFTARAN', 'alur-pendaftaran', '<p>PROSEDUR POE</p>', '', 0, 'Admin', '2016-12-22 08:46:11', '2016-12-22 08:46:11'),
(2, 'PROSEDUR', 'prosedur', '<ol class="prosedur">\r\n<li>Untuk membuka system PPDB Online direkomendasikan menggunakan:\r\n<ul>\r\n<li>Google Chrome terbaru, Jika tidak punya silahkan <a title="google" href="http://filehippo.com/download_google_chrome/" target="_blank">Klik disini</a> untuk mendownload</li>\r\n<li>Mozila Firefox terbaru, jika tidak punya silahkan <a title="Mozila" href="http://filehippo.com/download_firefox/" target="_blank">Klik disini</a> untuk mendownload</li>\r\n</ul>\r\n</li>\r\n<li>Jika menggunakan smartphone apabila tidak ada Google Chrome bisa menggunakan browser bawaan smartphone tersebut.</li>\r\n<li>Untuk membuka file pdf&nbsp;<span class="huruftebal">(formulir pendaftaran dan prosedur PPDB)</span>&nbsp;kami rekomendasikan menggunakan Foxit Reader. Jika tidak punya , silahkan&nbsp;<a href="http://filehippo.com/download_foxit/" target="_blank">Klik disini</a>&nbsp;untuk mendownload</li>\r\n</ol>\r\n<p>&nbsp;</p>', '', 0, 'Adminweb', '2016-12-22 08:46:39', '2016-12-23 12:57:03'),
(3, 'AGENDA', 'agenda', '<h4>Agenda PPDB&nbsp;MA AL Kahfi</h4>\r\n<h4>Tahun Pelajaran 2017/2018</h4>\r\n<table class="table table-condensed table-bordered table-striped">\r\n<thead>\r\n<tr>\r\n<th>&nbsp;No</th>\r\n<th>&nbsp;Agenda</th>\r\n<th>Tanggal</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>&nbsp;1</td>\r\n<td>&nbsp;Pendaftaran online</td>\r\n<td>&nbsp;1 - 16 Januari 2017</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;2</td>\r\n<td>&nbsp;Penyerahan Berkas</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;3</td>\r\n<td>&nbsp;Observasi Calon Siswa</td>\r\n<td>&nbsp;&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;4</td>\r\n<td>&nbsp;Wawancara Orang Tua</td>\r\n<td>&nbsp;&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;5</td>\r\n<td>&nbsp;Pengumuman</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;6</td>\r\n<td>&nbsp;Daftar Ulang</td>\r\n<td>\r\n<p>&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 0, 'Adminweb', '2016-12-22 08:47:05', '2016-12-23 12:58:28'),
(4, 'DASHBOARD ADMIN', 'dashboard-admin', '<p>Dashboard Admin</p>\r\n<p>ini dahsboard admin</p>', '', 0, 'Admin', '2016-12-23 12:51:13', '2016-12-31 05:35:18'),
(5, 'DASHBOARD USER', 'dashboard-user', '<p>Selamat, Anda behasil mendaftar di system kami, selanjutnya silakan melengkapi data pendaftaran yang sudah kami sediakan.&nbsp;</p>\r\n<ol>\r\n<li>Mengisi data siswa</li>\r\n<li>Mengisi data orang tua</li>\r\n<li>Download data setela di lengkapi</li>\r\n</ol>', '', 0, 'Administrator', '2016-12-23 12:51:35', '2016-12-31 05:42:02'),
(6, 'PENGUMUMAN', 'pengumuman', '<ol class="prosedur">\r\n<li>Untuk membuka system PPDB Online direkomendasikan menggunakan:\r\n<ul>\r\n<li>Google Chrome terbaru, Jika tidak punya silahkan <a title="google" href="http://filehippo.com/download_google_chrome/" target="_blank">Klik disini</a> untuk mendownload</li>\r\n<li>Mozila Firefox terbaru, jika tidak punya silahkan <a title="Mozila" href="http://filehippo.com/download_firefox/" target="_blank">Klik disini</a> untuk mendownload</li>\r\n</ul>\r\n</li>\r\n<li>Jika menggunakan smartphone apabila tidak ada Google Chrome bisa menggunakan browser bawaan smartphone tersebut.</li>\r\n<li>Untuk membuka file pdf&nbsp;<span class="huruftebal">(formulir pendaftaran dan prosedur PPDB)</span>&nbsp;kami rekomendasikan menggunakan Foxit Reader. Jika tidak punya , silahkan&nbsp;<a href="http://filehippo.com/download_foxit/" target="_blank">Klik disini</a>&nbsp;untuk mendownload</li>\r\n</ol>', '', 0, 'Adminweb', '2016-12-23 12:51:48', '2016-12-23 12:58:51');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `siswa_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '0', '2016-12-31 10:26:31', '2016-12-31 10:26:31'),
(2, 2, '0', '2016-12-31 10:40:06', '2016-12-31 10:40:06'),
(3, 3, '1', '2017-01-01 02:17:16', '2017-01-01 02:17:16'),
(4, 4, '1', '2017-01-02 06:36:40', '2017-01-02 06:36:40'),
(5, 5, '0', '2017-01-02 11:54:17', '2017-01-02 11:54:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$dlyEMx/PkzRUHYzTAUw5AuE1rTtaQdLTHQGLd1yFYKiLOKZwrl7yq', 'rJTj3gVGI9MeYU99glWicvMMQCCFqLYmAYiMHxHzqts9qLdyJaPbr9jMYvvc', '2016-12-31 10:23:58', '2017-01-02 07:49:22'),
(3, 'M. Yusron Adnan', 'yusron@gmail.com', '$2y$10$9E0b38zrn1TfkZFrfzxMQeTX7ebx7mJdvsqrLY0bVK0U9JOydQ/km', 'fUJyVS4xxEYZfxRoH62rx9Q1uS8ynWJPBukeJZZb7nEiVpCWp3jhZbOouYrP', '2016-12-31 10:24:54', '2017-01-02 14:01:39'),
(4, 'Fayza Ulya Azmina', 'azmina@gmail.com', '$2y$10$/d7v3V5R3TNYd3SJ.JAQUOgAMIVDlFlrLgaB7qwRY5gOUXDZTXoi6', 'nJaFggrMp0BLjoH1jZWbnnNjql0zV4uZXQI0m8QJKW6Q69F6vDJPjC5iWUaf', '2016-12-31 10:38:30', '2016-12-31 10:41:04'),
(5, 'Tri Achmad Syahroni', 'triachmadsyahroni@gmail.com', '$2y$10$nrX/laEizCJbvHi4.5WmweNUfDj1jmtXf9D50ydlzqyw4Qsmigtwa', 'WMaeUVFKMw50ya4u6mAzjbQK0zmexEzvCZhAhwRGLYDTT6GxienGvE1lslDk', '2017-01-01 02:15:28', '2017-01-01 02:24:18'),
(6, 'Raihan', 'raihan@gmail.com', '$2y$10$YhIn9.vYAR6Eapt27SHj5uSGwgtQ9kkZNjW6r.YdGoB8s100eIhpm', 't3GNak69T5rvt7hNRBMEElY27Ow3RNWpdMEz9HJ0tRH38cLCizcJNs4eAykG', '2017-01-02 06:33:30', '2017-01-02 06:53:31'),
(7, 'anies solo', 'anies@gmail.com', '$2y$10$pdbPQaeTgJDBNn6SYdJ7Ce0c7bv6z5/M7bPEutUdyHDoacsmDdZRO', 'U7jWMOHQFso8qdmlPbvTMMVxYTWsOrvE2NyCgMPrYiSHlolisadU8raAUlcz', '2017-01-02 06:43:29', '2017-01-02 06:48:49'),
(8, 'Santri Baru', 'santri@gmail.com', '$2y$10$wiguc0IfaTD9icgAedjc4uutPs2tjgVNA0GiOilVf2.4huzZJVW96', 'z2LGMbaPq8Cm6dBdJqsJ14le270KVtdjboWzTj25WlzRp2aSFj5eaoZybFAZ', '2017-01-02 11:52:25', '2017-01-02 12:11:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backups_user_id_foreign` (`user_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ortus`
--
ALTER TABLE `ortus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ortus_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswas`
--
ALTER TABLE `siswas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswas_user_id_foreign` (`user_id`);

--
-- Indexes for table `slideshows`
--
ALTER TABLE `slideshows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statis`
--
ALTER TABLE `statis`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `statis_slug_unique` (`slug`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `statuses_siswa_id_foreign` (`siswa_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `ortus`
--
ALTER TABLE `ortus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `siswas`
--
ALTER TABLE `siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `slideshows`
--
ALTER TABLE `slideshows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `statis`
--
ALTER TABLE `statis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `backups`
--
ALTER TABLE `backups`
  ADD CONSTRAINT `backups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ortus`
--
ALTER TABLE `ortus`
  ADD CONSTRAINT `ortus_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswas`
--
ALTER TABLE `siswas`
  ADD CONSTRAINT `siswas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `statuses`
--
ALTER TABLE `statuses`
  ADD CONSTRAINT `statuses_siswa_id_foreign` FOREIGN KEY (`siswa_id`) REFERENCES `siswas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
