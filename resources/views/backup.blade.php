@extends('style')
@section('content')
  <h3>Data Password</h3>
  <div class='table-responsive'>
    <table class='table table-striped table-bordered table-hover table-condensed'>
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Email</th>
          <th>Password</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($user as $d)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ ucfirst($d->name) }}</td>
            <td>{{ $d->email }}</td>
            <td>{{ $d->password }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
