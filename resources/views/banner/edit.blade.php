@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Update Banner</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($banner, ['route' => ['banner.update', $banner->id], 'method' => 'PUT', 'files'=>true, 'class'=>'form-horizontal']) !!}

          @include('banner.form')

          <div class="btn-group pull-right">
            <a href="{{ route('banner.index') }}" class="btn btn-warning">Batal</a>
            {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
