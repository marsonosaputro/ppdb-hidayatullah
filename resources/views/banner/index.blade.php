@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Daftar Banner</h3>
    </div>
    <div class="panel-body">
      <a href="{{ route('banner.create') }}" class="btn btn-sm btn-primary" style="margin-bottom: 10px;">Tambah</a>
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Judul</th>
              <th>URL</th>
              <th>Gambar</th>
              <th>Publish</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($banner as $d)
              {!! Form::open(array('url'=>'banner/'.$d->id, 'method'=>'delete')) !!}
              {!! Form::hidden('_delete', 'DELETE') !!}
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->judul }}</td>
                <td>{{ $d->url }}</td>
                <td><img src="{{ URL::asset('/images/banner/'.$d->image) }}" style="width:90px;" class="img img-responsive img-thumbnail" /></td>
                <td>
                  @if ($d->publish == 'Y')
                    <button type="button" class="btn btn-info btn-sm glyphicon glyphicon-check"></button>
                  @else
                    <button type="button" class="btn btn-warning btn-sm glyphicon glyphicon-minus"></button>
                  @endif
                </td>
                <td>
                  <a href="{{ url('banner/'.$d->id.'/edit') }}" class="btn btn-success btn-sm glyphicon glyphicon-pencil"></a>
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                </td>
                </td>
              </tr>
              {!! Form::close() !!}
            @endforeach
          </tbody>
        </table>
      </div>
      {!! $banner->render() !!}
    </div>
    
  </div>
@endsection
