@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">
        @role('admin')
          {{ $admin->judul }}
        @endrole
        @role('user')
          @if ($siswa && $siswa->status->status == '1')
            {!! $syarat->judul !!}
          @else
            {{ $user->judul }}
          @endif
        @endrole
      </h3>
    </div>
    <div class="panel-body">
      @role('admin')
      <i>Assalamualaikum, wr. wb.</i>
      <br><br>
        <p>
          <b>{{ Auth::user()->name }}</b>, Anda berada di dashboard Admin.
        </p>
        {!! $admin->content !!}
      @endrole
 {{-- ==============================================================================================  --}}
      @role('user')
        @if ($siswa && $siswa->status->status == '1')
          {!! $syarat->content !!}
        @else
          <i>Assalamualaikum, wr. wb.</i>
          <br><br>
            <p>
              <b>{{ Auth::user()->name }}</b>, Anda berada di dashboard User.
            </p>
            {!! $user->content !!}
        @endif
      @endrole
    </div>
  </div>

@endsection
