@extends('style')
@section('content')
  <h4>Data Pendaftar </h4>
  @if ($siswa)
    <div class='table-responsive'>
      <table class='table table-striped table-bordered table-hover table-condensed'>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Jenjang</th>
            <th>Tgl Daftar</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($siswa as $d)
            <tr>
              <td>{{ $no++ }}</td>
              <td>{{ $d->nama }}</td>
              <td>{{ $d->jenjang }}</td>
              <td>{{ $d->created_at }}</td>
              <td>
                <div class="btn-group">
                  @if ($d->status->status == 0)
                    <button type="button" class="btn btn-sm btn-default">BELUM DITERIMA</button>
                  @elseif ($d->status->status == 1)
                    <button type="button" class="btn btn-sm btn-success">DITERIMA</button>
                  @elseif ($d->status->status == 2)
                    <button type="button" class="btn btn-sm btn-danger">TIDAK DITERIMA</button>
                  @endif
                </div>
              </td>
              <td>
                <div class="btn-group">
                  <a href="{{ url('getpdf/'.$d->user_id) }}" class="btn btn-sm btn-primary"><i class="fa fa-file-pdf-o"> </i> &nbsp; CETAK</a>
                  <a href="{{ url('viewdata/'.$d->user_id) }}" class="btn btn-sm btn-info"><i class="fa fa-folder-open"> </i> &nbsp; VIEW</a>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="pull-left">
      {!! $siswa->render() !!}
    </div>
    <div class="pull-right">
      <a href="{{ url('downloadexcel/'.Request::segment(2)) }}" class="btn btn-sm btn-success"><i class="fa fa-file-excel-o"></i> &nbsp;DOWNLOAD DATA</a>
    </div>
  @else
    <h4>Belum ada pendaftar</h4>
  @endif
@endsection
