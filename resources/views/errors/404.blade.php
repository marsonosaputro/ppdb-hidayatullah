@extends('style')
@section('content')
  <div class="alert alert-dismissible alert-info">
    <h3>Halaman tidak ditemukan!!!</h3>
    <hr>
    <h4 class="text-center">Halaman yang Anda tuju tidak ditemukan.</h4>
    <p class="text-center">
      <a href="{{ url('/') }}" class="btn btn-warning">HOME</a>
    </p>
    <hr>
    <br>
  </div>
@endsection
