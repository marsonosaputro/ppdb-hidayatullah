<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data PDF</title>
    <!-- Styles -->
    <link href="{{ asset('css/pdf.css') }}" rel="stylesheet">


  </head>
  <body>
    @if ($siswa)
      @if ($siswa->jenjang == 'MTS')
        <img src="{{ asset('images/'.setting()->logo) }}" style="width: 100%; margin-top: -20px;"  />
      @elseif ($siswa->jenjang == 'MA')
        <img src="{{ asset('images/'.setting()->logoma) }}" style="width: 100%; margin-top: -20px;" />
      @endif
    @endif
  <br>
    <h3 class="text text-center">Formulir Pendaftaran</h3>
    <h4>Data Calon Santri</h4>
    @if ($siswa)
    <div class='table-responsive'>
      <table class='table table-bordered table-condensed'>
        <tbody>
          <tr>
            <td style="width:30%">Jenjang</td><td style="width:50%">{{ $siswa->jenjang}}</td>
            <td rowspan="10">
              @if ($siswa->foto)
                <img src="{{ asset('foto/'.$siswa->foto) }}" />
              @endif
            </td>
          </tr>
          <tr>
            <td>Nama Lengkap</td><td>{{ $siswa->nama }}</td>
          </tr>
          <tr>
            <td>Tempat Tanggal Lahir</td><td>{{ $siswa->tempatlhr }}, {{ tgl_indo($siswa->tgllahir) }}</td>
          </tr>
          <tr>
            <td>Alamat Lengkap</td><td>{{ $siswa->alamatlkp }}</td>
          </tr>
          <tr>
            <td>Kode Pos</td><td>{{ $siswa->kodepos }}</td>
          </tr>
          <tr>
            <td>Asal Sekolah</td><td>{{ $siswa->asal }}</td>
          </tr>
          <tr>
            <td>Alamat Sekolah</td><td>{{ $siswa->alamatskl }}</td>
          </tr>
          <tr>
            <td>Tahun Lulus</td><td>{{ $siswa->thnlulus }}</td>
          </tr>
          <tr>
            <td>No. HP</td><td>{{ $siswa->hp }}</td>
          </tr>
          <tr>
            <td>Email</td><td>{{ $siswa->email }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    @else
      <div class="alert alert-danger">
        <h4 class="text text-center">Silakan di lengkapi terlebih dahulu data siswanya!</h4>
      </div>
    @endif

    <h4>Data Orang Tua</h4>
    @if ($ortu)
    <div class='table-responsive'>
      <table class='table table-striped table-bordered table-hover table-condensed'>
        <tbody>
          <tr>
            <td style="width:30%">Nama Ayah</td><td>{{ $ortu->ayah }}</td>
          </tr>
          <tr>
            <td>Nama Ibu</td><td>{{ $ortu->ibu }}</td>
          </tr>
          <tr>
            <td>Nama Wali</td><td>{{ $ortu->wali }}</td>
          </tr>
          <tr>
            <td>Alamat Rumah</td><td>{{ $ortu->alamatrmh }}</td>
          </tr>
          <tr>
            <td>Profesi</td><td>{{ $ortu->profesi }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    @role('user')

      <table class='table table-condensed'>
        <tbody>
          <tr>
            <td class="text-center">
              <br>
              Mengetahui, <br>Orang Tua / Wali Santri <br><br><br><br>________________________
            </td>
            <td class="text-center">
              <br>
              .........................., .........................<br><br><br><br><br><u>{{ $siswa->nama }}</u>
            </td>
          </tr>
        </tbody>
      </table>
    @endrole

    @else
      <div class="alert alert-danger">
        <h4 class="text text-center">Data orang tua belum di lengkapi.</h4>
      </div>
    @endif

    @role('user')
      <div style="page-break-before: always;"></div>
      <br>
      <h3 class="text text-center">Pengumuman</h3>
      {!! $pengumuman->content !!}
    @endrole

  </body>
</html>
