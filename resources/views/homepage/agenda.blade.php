@extends('style')
@section('content')
  <hr>
  <ol class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li>Agenda</li>
  </ol>
  <hr>

  <h4>{{ $agenda->judul }}</h4>
  {!! $agenda->content !!}
@endsection
