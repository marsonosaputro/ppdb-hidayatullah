@extends('style')
@section('content')
  <hr>
  <ol class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li>Prosedur</li>
  </ol>
  <hr>
  <h4>{{ $prosedur->judul }}</h4>
  {!! $prosedur->content !!}
@endsection
