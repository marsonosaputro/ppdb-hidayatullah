<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Data Orang Tua</h3>
  </div>
  <div class="panel-body">
    <div class='table-responsive'>
      <table class='table table-striped table-bordered table-hover table-condensed'>
        <tbody>
          <tr>
            <td>Nama Ayah</td><td>{{ ucfirst($ortu->ayah) }}</td>
          </tr>
          <tr>
            <td>Nama Ibu</td><td>{{ ucfirst($ortu->ibu) }}</td>
          </tr>
          <tr>
            <td>Nama Wali</td><td>{{ ucfirst($ortu->wali) }}</td>
          </tr>
          <tr>
            <td>Alamat Rumah</td><td>{{ ucfirst($ortu->alamatrmh) }}</td>
          </tr>
          <tr>
            <td>Profesi</td><td>{{ ucfirst($ortu->profesi) }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="pull-right">
      <a href="{{ route('ortu.edit', $ortu->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"> </i> EDIT</a>
    </div>
  </div>
</div>
