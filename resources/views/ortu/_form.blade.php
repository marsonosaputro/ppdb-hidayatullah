<div class="form-group{{ $errors->has('ayah') ? ' has-error' : '' }}">
    {!! Form::label('ayah', 'Nama Ayah', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('ayah', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('ayah') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('ibu') ? ' has-error' : '' }}">
    {!! Form::label('ibu', 'Nama Ibu', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('ibu', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('ibu') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('wali') ? ' has-error' : '' }}">
    {!! Form::label('wali', 'Nama Wali', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('wali', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('wali') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('alamatrmh') ? ' has-error' : '' }}">
    {!! Form::label('alamatrmh', 'Alamat Rumah', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('alamatrmh', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('alamatrmh') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('profesi') ? ' has-error' : '' }}">
    {!! Form::label('profesi', 'Profesi', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('profesi', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('profesi') }}</small>
    </div>
</div>
{!! Form::hidden('user_id', Auth::user()->id) !!}
