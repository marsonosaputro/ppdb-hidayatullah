<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Formulir Data Orang Tua</h3>
  </div>
  <div class="panel-body">
    {!! Form::open(['method' => 'POST', 'route' => 'ortu.store', 'class' => 'form-horizontal']) !!}

        @include('ortu._form')

        <div class="btn-group pull-right">
            {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
        </div>
    {!! Form::close() !!}
  </div>
</div>
