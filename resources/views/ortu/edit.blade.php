@extends('style')
@section('content')
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Data Orang Tua</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($ortu, ['route' => ['ortu.update', $ortu->id], 'class' => 'form-horizontal', 'method' => 'PUT']) !!}

          @include('ortu._form')

          <div class="btn-group pull-right">
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>

      {!! Form::close() !!}
    </div>
  </div>
@endsection
