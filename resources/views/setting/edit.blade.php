@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Setting App PPDB</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($setting, ['route' => ['setting.update', $setting->id], 'class' => 'form-horizontal', 'files'=>true, 'method' => 'PUT']) !!}

          <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              {!! Form::label('title', 'Tittle', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('title', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('title') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('description') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
              {!! Form::label('author', 'Author', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('author', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('author') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('keyword') ? ' has-error' : '' }}">
              {!! Form::label('keyword', 'Keyword', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('keyword', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('keyword') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
              {!! Form::label('contact', 'Contact', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('contact') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('contact1') ? ' has-error' : '' }}">
              {!! Form::label('contact1', 'Contact 1', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('contact1', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('contact1') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('contact2') ? ' has-error' : '' }}">
              {!! Form::label('contact2', 'Contact 2', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('contact2', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('contact2') }}</small>
              </div>
          </div>

          @if ($setting->logo)
            <div class="form-group">
                {!! Form::label('logo', 'Kop MTS Sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    <img src="{{ asset('images/'.$setting->logo) }}" class="img img-responsive" />
                </div>
            </div>
          @endif

          <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
              {!! Form::label('logo', 'Ganti Kop MTS', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                      {!! Form::file('logo', ['class' => 'form-control']) !!}
                      <p class="help-block">Size: 850 x 236px</p>
                      <small class="text-danger">{{ $errors->first('logo') }}</small>
                  </div>
          </div>

          @if ($setting->logoma)
            <div class="form-group">
                {!! Form::label('logo', 'Kop MA Sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    <img src="{{ asset('images/'.$setting->logoma) }}" class="img img-responsive" />
                </div>
            </div>
          @endif

          <div class="form-group{{ $errors->has('logoma') ? ' has-error' : '' }}">
              {!! Form::label('logoma', 'Ganti Kop MA', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                      {!! Form::file('logoma', ['class' => 'form-control']) !!}
                      <p class="help-block">Size: 850 x 236px</p>
                      <small class="text-danger">{{ $errors->first('logoma') }}</small>
                  </div>
          </div>

          <div class="btn-group pull-right">
              <a href="{{ route('setting.index') }}" class="btn btn-warning">BATAL</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>

      {!! Form::close() !!}
    </div>
  </div>
@endsection
