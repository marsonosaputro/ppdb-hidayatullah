@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Setting PPDB Online</h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <tbody>
            <tr>
              <td>Title</td><td>{{ $setting->title }}</td>
            </tr>
            <tr>
              <td>Description</td><td>{{ $setting->description }}</td>
            </tr>
            <tr>
              <td>Author</td><td>{{ $setting->author }}</td>
            </tr>
            <tr>
              <td>Keyword</td><td>{{ $setting->keyword }}</td>
            </tr>
            <tr>
              <td>Contact</td><td>{{ $setting->contact }}</td>
            </tr>
            <tr>
              <td>Contact 1</td><td>{{ $setting->contact1 }}</td>
            </tr>
            <tr>
              <td>Contact 2</td><td>{{ $setting->contact2 }}</td>
            </tr>
            <tr>
              <td>Kop Surat/Logo MTs</td>
              <td>

              </td>
            </tr>
            <tr>
              <td colspan="2">
                @if ($setting->logo)
                  <img src="{{ asset('images/'.$setting->logo) }}" class="img img-responsive" />
                @endif
              </td>
            </tr>
            <tr>
              <td>Kop Surat/Logo MA</td>
              <td>

              </td>
            </tr>
            <tr>
              <td colspan="2">
                @if ($setting->logoma)
                  <img src="{{ asset('images/'.$setting->logoma) }}" class="img img-responsive" />
                @endif
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="pull-right">
        <a href="{{ route('setting.edit', $setting->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> &nbsp; EDIT</a>
      </div>
    </div>

  </div>
@endsection
