@role('admin')
  <div class="list-group">
    <a href="#" class="list-group-item list-group-item-warning active">MENU PPDB</a>
    <a href="{{ url('datapendaftar/mts') }}" class="list-group-item"><i class="fa fa-check-square"></i>&nbsp; Data Pendaftar MTs</a>
    <a href="{{ url('datapendaftar/ma') }}" class="list-group-item"><i class="fa fa-check-square"></i>&nbsp; Data Pendaftar MA</a>
    <a href="{{ url('backup') }}" class="list-group-item"><i class="fa fa-key"></i>&nbsp; Backup Password</a>
  </div>

  <div class="list-group">
    <a href="#" class="list-group-item list-group-item-warning active"> Pengaturan Website </a>
    <a href="{{ url('statis') }}" class="list-group-item"> <i class="fa fa-file-text" aria-hidden="true"></i> &nbsp; Statis </a>
    <a href="{{ url('slide') }}" class="list-group-item"> <i class="fa fa-file-image-o" aria-hidden="true"></i> &nbsp; Slideshow </a>
    <a href="{{ url('banner') }}" class="list-group-item"> <i class="fa fa-desktop" aria-hidden="true"></i> &nbsp; Banner </a>
    <a href="{{ url('user') }}" class="list-group-item"> <i class="fa fa-user-md" aria-hidden="true"></i> &nbsp; Admin </a>
    <a href="{{ url('setting') }}" class="list-group-item"> <i class="fa fa-gears" aria-hidden="true"></i> &nbsp; Setting </a>
  </div>

@endrole

@role('user')
@if ($siswa)
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">STATUS</h3>
    </div>
    <div class="panel-body text-center">
      @if ($siswa->status->status == 0)
        <button type="button" class="btn btn-sm btn-default">BELUM DITERIMA</button>
      @elseif ($siswa->status->status == 1)
        <button type="button" class="btn btn-sm btn-success">DITERIMA</button>
      @elseif ($siswa->status->status == 2)
        <button type="button" class="btn btn-sm btn-danger">TIDAK DITERIMA</button>
      @endif
    </div>
  </div>
@endif

  <div class="list-group">
    <a href="#" class="list-group-item list-group-item-success active"> MENU UTAMA </a>
    <a href="{{ url('siswa') }}" class="list-group-item"> <i class="fa fa-check-square" aria-hidden="true"></i> &nbsp;Data Siswa </a>
    <a href="{{ url('ortu') }}" class="list-group-item"> <i class="fa fa-check-square" aria-hidden="true"></i> &nbsp;Data Ortu </a>
    <a href="{{ url('datalengkap') }}" class="list-group-item"> <i class="fa fa-check-square" aria-hidden="true"></i> &nbsp;View Data Lengkap </a>
    <a href="{{ url('getpdf') }}" class="list-group-item"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> &nbsp;Download </a>
  </div>
@endrole


<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Online Service</h3>
  </div>
  <div class="panel-body">
    <p>Jika mengalami kesulitan silahkan kontak dengan petugas kami di bawah ini: </p>
    {!! !empty(setting()->contact) ?  '<p><i class="fa fa-whatsapp"> </i> '.setting()->contact.' </p>' : '' !!}
    {!! !empty(setting()->contact1) ?  '<p><i class="fa fa-whatsapp"> </i> '.setting()->contact2.' </p>' : '' !!}
    {!! !empty(setting()->contact2) ?  '<p><i class="fa fa-whatsapp"> </i> '.setting()->contact2.' </p>' : '' !!}
  </div>
</div>

@if ($banner)
  @foreach ($banner as $d)
    <a href="{{ url($d->url) }}">
      <img src="{{ asset('images/banner/'.$d->image) }}" class="img img-responsive" />
    </a>
  @endforeach
@endif
