<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Data Siswa</h3>
  </div>
  <div class="panel-body">
    <div class='table-responsive'>
      <table class='table table-striped table-bordered table-hover table-condensed'>
        <tbody>
          <tr>
            <td>Jenjang</td><td>{{ $siswa->jenjang }}</td>
            <td rowspan="12">
              @if ($siswa->foto)
                <img src="{{ asset('foto/'.$siswa->foto) }}" class="img img-responsive img-thumbnail" />
              @endif
            </td>
          </tr>
          <tr>
            <td>Nama Lengkap</td><td>{{ ucfirst($siswa->nama) }}</td>
          </tr>
          <tr>
            <td>Tempat Tanggal Lahir</td><td>{{ ucfirst($siswa->tempatlhr) }}, {{ tgl_indo($siswa->tgllahir) }}</td>
          </tr>
          <tr>
            <td>Alamat Lengkap</td><td>{{ ucfirst($siswa->alamatlkp) }}</td>
          </tr>
          <tr>
            <td>Kode Pos</td><td>{{ $siswa->kodepos }}</td>
          </tr>
          <tr>
            <td>Asal Sekolah</td><td>{{ $siswa->asal }}</td>
          </tr>
          <tr>
            <td>Alamat Sekolah</td><td>{{ ucfirst($siswa->alamatskl) }}</td>
          </tr>
          <tr>
            <td>Tahun Lulus</td><td>{{ $siswa->thnlulus }}</td>
          </tr>
          <tr>
            <td>No. HP</td><td>{{ $siswa->hp }}</td>
          </tr>
          <tr>
            <td>Email</td><td>{{ $siswa->email }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="pull-right">
      <a href="{{ route('siswa.edit', $siswa->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"> </i> EDIT</a>
    </div>
  </div>

</div>
