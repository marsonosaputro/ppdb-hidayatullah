<div class="form-group{{ $errors->has('jenjang') ? ' has-error' : '' }}">
    {!! Form::label('jenjang', 'Pilih Jenjang', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('jenjang', [''=>'', 'MA'=>'MA', 'MTS'=>'MTS'], null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('jenjang') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
    {!! Form::label('nama', 'Nama Lengkap', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('nama', Auth::user()->name, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('nama') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('tempatlhr') ? ' has-error' : '' }}">
    {!! Form::label('tempatlhr', 'Tempat Lahir', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('tempatlhr', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('tempatlhr') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('tgllahir') ? ' has-error' : '' }}">
    {!! Form::label('tgllahir', 'Tanggal Lahir', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('tgllahir', !empty($siswa->tgllahir) ? tgl_indo($siswa->tgllahir) : null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('tgllahir') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('alamatlkp') ? ' has-error' : '' }}">
    {!! Form::label('alamatlkp', 'Alamat Lengkap', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('alamatlkp', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('alamatlkp') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('kodepos') ? ' has-error' : '' }}">
    {!! Form::label('kodepos', 'Kode Pos', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('kodepos', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('kodepos') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('asal') ? ' has-error' : '' }}">
    {!! Form::label('asal', 'Asal Sekolah', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('asal', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('asal') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('alamatskl') ? ' has-error' : '' }}">
    {!! Form::label('alamatskl', 'Alamat Sekolah', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('alamatskl', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('alamatskl') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('thnlulus') ? ' has-error' : '' }}">
    {!! Form::label('thnlulus', 'Tahun Lulus', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('thnlulus', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('thnlulus') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('hp') ? ' has-error' : '' }}">
    {!! Form::label('hp', 'No. HP', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('hp', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('hp') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('email', Auth::user()->email, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('email') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
    {!! Form::label('foto', 'File Foto', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            {!! Form::file('foto', ['class' => 'form-control']) !!}
            <!-- <p class="help-block">Help block text</p> -->
            <small class="text-danger">{{ $errors->first('foto') }}</small>
        </div>
</div>
{!! Form::hidden('user_id', Auth::user()->id) !!}
