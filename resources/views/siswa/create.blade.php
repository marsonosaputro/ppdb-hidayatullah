<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Formulir Data Siswa</h3>
  </div>
  <div class="panel-body">
    {!! Form::open(['method' => 'POST', 'route' => 'siswa.store', 'files'=>true,'class' => 'form-horizontal']) !!}

        @include('siswa._form')

        <div class="btn-group pull-right">
            {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
        </div>
    {!! Form::close() !!}
  </div>
</div>
