@extends('style')
@section('content')
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h3 class="panel-title">Data Siswa</h3>
    </div>
    <div class="panel-body">
      @if ($siswa)
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <tbody>
            <tr>
              <td>STATUS</td>
              <td>
                @if ($siswa->status->status == 0)
                  <button type="button" class="btn btn-sm btn-default">BELUM DITERIMA</button>
                @elseif ($siswa->status->status == 1)
                  <button type="button" class="btn btn-sm btn-success">DITERIMA</button>
                @elseif ($siswa->status->status == 2)
                  <button type="button" class="btn btn-sm btn-danger">TIDAK DITERIMA</button>
                @endif
              </td>
              <td rowspan="12">
                @if ($siswa->foto)
                  <img src="{{ asset('foto/'.$siswa->foto) }}" class="img img-responsive img-thumbnail" />
                @endif
              </td>
            </tr>
            <tr>
              <td>Jenjang</td><td>{{ $siswa->jenjang }}</td>
            </tr>
            <tr>
              <td>Nama Lengkap</td><td>{{ ucfirst($siswa->nama) }}</td>
            </tr>
            <tr>
              <td>Tempat Tanggal Lahir</td><td>{{ ucfirst($siswa->tempatlhr) }}, {{ tgl_indo($siswa->tgllahir) }}</td>
            </tr>
            <tr>
              <td>Alamat Lengkap</td><td>{{ ucfirst($siswa->alamatlkp) }}</td>
            </tr>
            <tr>
              <td>Kode Pos</td><td>{{ $siswa->kodepos }}</td>
            </tr>
            <tr>
              <td>Asal Sekolah</td><td>{{ $siswa->asal }}</td>
            </tr>
            <tr>
              <td>Alamat Sekolah</td><td>{{ ucfirst($siswa->alamatskl) }}</td>
            </tr>
            <tr>
              <td>Tahun Lulus</td><td>{{ $siswa->thnlulus }}</td>
            </tr>
            <tr>
              <td>No. HP</td><td>{{ $siswa->hp }}</td>
            </tr>
            <tr>
              <td>Email</td><td>{{ $siswa->email }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="pull-right">
        <a href="{{ route('siswa.edit', $siswa->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"> </i> EDIT</a>
      </div>
      @else
        <h4>Data Siswa belum dilengkapi <a href="{{ url('siswa') }}" class="btn btn-sm btn-info">Lengkapi Data</a></h4>
      @endif
    </div>

  </div>

  <div class="panel panel-warning">
    <div class="panel-heading">
      <h3 class="panel-title">Data Orang Tua</h3>
    </div>
    <div class="panel-body">
      @if ($ortu)
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <tbody>
            <tr>
              <td>Nama Ayah</td><td>{{ ucfirst($ortu->ayah) }}</td>
            </tr>
            <tr>
              <td>Nama Ibu</td><td>{{ ucfirst($ortu->ibu) }}</td>
            </tr>
            <tr>
              <td>Nama Wali</td><td>{{ ucfirst($ortu->wali) }}</td>
            </tr>
            <tr>
              <td>Alamat Rumah</td><td>{{ ucfirst($ortu->alamatrmh) }}</td>
            </tr>
            <tr>
              <td>Profesi</td><td>{{ ucfirst($ortu->profesi) }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="pull-right">
        <a href="{{ url('getpdf') }}" class="btn btn-sm btn-success"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> &nbsp;DOWNLOAD </a>
        <a href="{{ route('ortu.edit', $ortu->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"> </i> EDIT</a>
      </div>
      @else
        <h4>Data Orang Tua belum dilengkapi <a href="{{ url('ortu') }}" class="btn btn-sm btn-info">Lengkapi Data</a></h4>
      @endif
    </div>
  </div>

@endsection
