@extends('style')
@section('content')

<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Edit Data Siswa</h3>
  </div>
  <div class="panel-body">
    {!! Form::model($siswa, ['route' => ['siswa.update', $siswa->id], 'files'=>true, 'class' => 'form-horizontal', 'method' => 'PUT']) !!}

        @include('siswa._form')

        <div class="btn-group pull-right">
            {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
        </div>

    {!! Form::close() !!}
  </div>
</div>


@endsection
