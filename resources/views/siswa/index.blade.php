@extends('style')
@section('content')
  @if ($siswa)
    @include('siswa._datasiswa')
  @else
    @include('siswa.create')
  @endif
@endsection
