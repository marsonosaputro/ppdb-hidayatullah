@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Slideshow</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($slide, ['route' => ['slide.update', $slide->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files'=>true]) !!}

          @include('slideshow.form')

          <div class="btn-group pull-right">
              <a href="{{ route('slide.index') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
