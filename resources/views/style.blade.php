<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ setting()->description }}">
  	<meta name="author" content="{{ setting()->author }}">
  	<meta name="keyword" content="{{ setting()->keyword }}">
    <title>{{ setting()->title }}</title>

    <!-- Bootstrap -->
    <!-- Styles -->
    <link href="{{ asset('style/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('style/css/bootswatch.less') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('style/css/_variables.scss') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('style/css/bootstrap.css') }}" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="{{ asset('style/css/font-awesome.min.css') }}">
    <!-- DATEPICKER STYLE -->
    <link rel="stylesheet" href="{{ asset('development-bundle/themes/base/ui.all.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- TOP MENU -->
    <nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="{{ url('/') }}">PPDB</a>
  </div>

  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="{{ url('/') }}">Home</a></li>
      <li><a href="{{ url('prosedur') }}">Prosedur</a></li>
      <li><a href="{{ url('agenda') }}">Agenda</a></li>
      @if (Auth::check())
        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
        <li><a href="{{ url('logout') }}">LOGOUT</a></li>
      @else
        <li><a href="{{ route('register') }}">Pendaftaran</a></li>
        <li><a href="{{ route('login') }}">LOGIN</a></li>
      @endif

    </ul>

    <ul class="nav navbar-nav navbar-right">

    </ul>
  </div>
</div>
</nav>
    <!-- END TOP MENU -->
<div style="clear:both; height:70px"></div>
    <!-- MAIN -->
    <div class='container'>
      <div class="row">
        <div class="col-md-9">
          @yield('content')
        </div>
        <div class="col-md-3">
          @include('sidebar')
        </div>
      </div>
    </div>
    <!-- END MAIN -->
    <!-- FOOTER -->
    <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copy">
                    <hr style="margin-bottom: -5px;" />
    				            <h5>Studio17 <small>Copyright &copy; 2017</small></h5>
                </div>
            </div>
        </div>
    </div>
    </footer>
    <!-- JS -->
    <script src="{{ asset('style/js/jquery-1.11.3.js')}}"></script>
    <script src="{{ asset('style/js/bootstrap.min.js')}}"></script>

    <!-- jQuery DATE-->


    <script type="text/javascript" src="{{ asset('development-bundle/jquery-1.3.2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('development-bundle/ui/ui.core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('development-bundle/ui/ui.datepicker.js') }}"></script>

    <script type="text/javascript" src="{{ asset('development-bundle/ui/i18n/ui.datepicker-id.js') }}"></script>

        <script type="text/javascript">
          $(document).ready(function(){
            $("#tgllahir").datepicker({
              dateFormat  : "dd MM yy",
              changeMonth : true,
              changeYear  : true,
              yearRange   : "-20:+5"
            });
          });
        </script>


  </body>
</html>
