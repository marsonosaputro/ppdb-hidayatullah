@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Update User</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PUT', 'class'=>'form-horizontal']) !!}

          @include('user.form')

          <div class="btn-group pull-right">
              <a href="{{ route('user.index') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
