@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Data Admin </h3>
    </div>
    <div class="panel-body">
    @include('notif')
    <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary" style="margin-bottom:10px">Tambah</a>
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Lengkap</th>
              <th>Email</th>
              <th>Sejak</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($user as $d)
              {!! Form::open(array('url'=>'user/'.$d->id, 'method'=>'delete')) !!}
              {!! Form::hidden('_delete', 'DELETE') !!}
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->email }}</td>
                <td>{{ $d->created_at }}</td>
                <td>
                  <a href="{{ url('user/'.$d->id.'/edit') }}" class="btn btn-success btn-sm glyphicon glyphicon-pencil"></a>
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                </td>
              </tr>
              {!! Form::close() !!}
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    
  </div>
@endsection
