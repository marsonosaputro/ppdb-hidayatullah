@extends('style')
@section('content')
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Data Siswa</h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <tbody>
            <tr>
              <td>STATUS</td>
              <td>
                @if ($siswa->status->status == 0)
                  <button type="button" class="btn btn-sm btn-default">BELUM DITERIMA</button>
                @elseif ($siswa->status->status == 1)
                  <button type="button" class="btn btn-sm btn-success">DITERIMA</button>
                @elseif ($siswa->status->status == 2)
                  <button type="button" class="btn btn-sm btn-danger">TIDAK DITERIMA</button>
                @endif
              </td>
              <td rowspan="11">
                @if ($siswa->foto)
                  <img src="{{ asset('foto/'.$siswa->foto) }}" class="img img-responsive img-thumbnail" />
                @endif
              </td>
            </tr>
            <tr>
              <td>Jenjang</td><td>{{ $siswa->jenjang }}</td>
            </tr>
            <tr>
              <td>Nama Lengkap</td><td>{{ ucfirst($siswa->nama) }}</td>
            </tr>
            <tr>
              <td>Tempat Tanggal Lahir</td><td>{{ ucfirst($siswa->tempatlhr) }}, {{ tgl_indo($siswa->tgllahir) }}</td>
            </tr>
            <tr>
              <td>Alamat Lengkap</td><td>{{ ucfirst($siswa->alamatlkp) }}</td>
            </tr>
            <tr>
              <td>Kode Pos</td><td>{{ $siswa->kodepos }}</td>
            </tr>
            <tr>
              <td>Asal Sekolah</td><td>{{ $siswa->asal }}</td>
            </tr>
            <tr>
              <td>Alamat Sekolah</td><td>{{ ucfirst($siswa->alamatskl) }}</td>
            </tr>
            <tr>
              <td>Tahun Lulus</td><td>{{ $siswa->thnlulus }}</td>
            </tr>
            <tr>
              <td>No. HP</td><td>{{ $siswa->hp }}</td>
            </tr>
            <tr>
              <td>Email</td><td>{{ $siswa->email }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

  </div>

  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Data Orang Tua</h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <tbody>
            <tr>
              <td>Nama Ayah</td><td>{{ ucfirst($ortu->ayah) }}</td>
            </tr>
            <tr>
              <td>Nama Ibu</td><td>{{ ucfirst($ortu->ibu) }}</td>
            </tr>
            <tr>
              <td>Nama Wali</td><td>{{ ucfirst($ortu->wali) }}</td>
            </tr>
            <tr>
              <td>Alamat Rumah</td><td>{{ ucfirst($ortu->alamatrmh) }}</td>
            </tr>
            <tr>
              <td>Profesi</td><td>{{ ucfirst($ortu->profesi) }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
<div class="pull-left">
  {!! Form::open(['method' => 'POST', 'url' => 'ubahstatus', 'class'=>'form-inline']) !!}
      {!! Form::hidden('siswa_id', $siswa->id) !!}
      {!! Form::hidden('user_id', $siswa->user_id) !!}
      {!! Form::label('status', 'STATUS : &nbsp;') !!}
      {!! Form::select('status', ['0'=>'BELUM DITERIMA', '1'=>'DITERIMA', '2'=>'TIDAK DITERIMA'], $siswa->status->status, ['class'=>'form-control','onchange' => 'this.form.submit()']) !!}
  {!! Form::close() !!}
</div>
<div class="pull-right">

  <div class="btn-group">
    <a href="{{ URL::previous()  }}" class="btn btn-sm btn-info"><i class="fa fa-backward"></i> &nbsp;BACK</a>
    <a href="{{ url('getpdf/'.$siswa->user_id) }}" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> &nbsp;CETAK</a>
  </div>
</div>
@endsection
