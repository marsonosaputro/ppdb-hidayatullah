<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SiswaController;

use App\Setting;
use App\Siswa;
use App\Banner;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/prosedur', 'HomeController@prosedur');
Route::get('/agenda', 'HomeController@agenda');

Auth::routes();

Route::group(['middleware'=>['auth']], function()
{
  Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
  Route::resource('siswa', 'SiswaController');
  Route::resource('ortu', 'OrtuController');
  Route::get('datalengkap', 'SiswaController@datalengkap');
  Route::get('getpdf/{id?}', 'SiswaController@getPdf');
  Route::get('dashboard', 'HomeController@dashboard');

});

Route::group(['middleware'=>['auth', 'role:admin']], function()
{
  Route::resource('statis', 'StatisController');
  Route::resource('slide', 'SlideshowController');
  Route::resource('banner', 'BannerController');
  Route::resource('user', 'UserController');
  Route::resource('setting', 'SettingController');
  Route::get('datapendaftar/{jenjang?}', 'HomeController@dataPendaftar');
  Route::get('backup', 'HomeController@backup');
  Route::get('viewdata/{id?}', 'HomeController@viewData');
  Route::post('ubahstatus', 'HomeController@ubahStatus');
  Route::get('downloadexcel/{jenjang?}', 'HomeController@downloadExcel');
});


View::composer('sidebar', function($view){
  $view->banner = App\Banner::where('publish', 'Y')->get();
  if (Auth::check())
  {
    $view->siswa = App\Siswa::where('user_id',Auth::user()->id)->first();
  }
});
